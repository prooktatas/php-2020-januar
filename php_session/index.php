<?php

/*
* User autentikálás
* Session kezelés
*/

$method = $_SERVER['REQUEST_METHOD'];
$parsedUrl = parse_url($_SERVER['REQUEST_URI']);
$path = $parsedUrl['path'];

if ($method === 'GET' && $path === '/') {
    require './login.html';
}

if ($method === 'POST' && $path === '/login') {
    session_start();
    $_SESSION['username'] = $_POST['username'];
    header('Location: /vedett-eroforras');
}

if($method === 'GET' && $path === '/vedett-eroforras') {
    session_start();
    if(isset($_SESSION['username'])) {
        echo '<h1>Védett erőforrás, szia ' . $_SESSION['username'] .'</h1>
        <a href="/logout">Kijelentkezés</a>';
    } else {
        header('Location: /');
    }
}

if($method === 'GET' && $path === '/logout') {
    session_start();
    session_destroy();
    header('Location: /');
}

if($method === 'GET' && $path === '/webshop') {
    $kosarTartalma = isset($_COOKIE['kosarTartalma']) ? $_COOKIE['kosarTartalma'] : 0; 
    echo '
        <form action="/product-to-basket" method="post">
            <button>Termék kosárba helyezése</button>
        </form>
        <h1>Kosár tartalma: ' . $kosarTartalma .'</h1>
    ';
}

if($method === 'POST' && $path === '/product-to-basket') {
    setcookie('kosarTartalma', 1, time() + 60 * 60 * 24);
    header('Location: /webshop');
}





