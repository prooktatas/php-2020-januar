

document.getElementById('fetch-book').onclick = function () {
    var data = null;
    
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;
    
    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        document.getElementById('book-component').innerHTML = this.responseText;
      }
    });
    
    xhr.open("GET", "http://localhost:8888/books/6");
    xhr.setRequestHeader("cache-control", "no-cache");
    xhr.setRequestHeader("Postman-Token", "46d45d75-dc0d-41d4-9e20-a250b7cb4c5c");
    
    xhr.send(data);

}
