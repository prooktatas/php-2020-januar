<?php
/*
* Composer
* package manager
*/

require('./vendor/autoload.php');

$dispatcher = FastRoute\simpleDispatcher(function($r) {
    $r->addRoute('GET', '/', function ($query, $body, $vars) {
        echo 'Címlap 
        <div id="book-component"></div>
        <button id="fetch-book">Könyv lekérése</button>
        <script src="/public/script.js"></script>';
    });

    // REST API (representational state transfer)
        // adat JSON encoded formában utazik a body-ban
        // URL-ek azok a következő séma szerint épülnek fel:
        
        // books erőforráson engedünk végrehajtani CRUD operációkat
        // Listázás:                                  'GET',     '/books'
        // Lekérdezés egyedi azonosító alapján:       'GET',     '/books/<id>'
        // Erőforrás létrehozása:                     'POST',    '/books'
        // Törlés                                     'DELETE'   '/books/<id>' -> {id: <id>}
        // Update                                     'PUT'      '/books/<id>'


    $r->addRoute('GET', '/books/{azonosito:\d+}', function ($query, $body, $vars) {
        $id = $vars['azonosito'];
        // SELECT * FROM books WHERE id = <id>

        $book = [
            'id' => 6,
            'author' => 'JK Rowling',
            'title' => 'Harry Potter and the philosopher\'s stone'
        ];

        header('Content-type: application/json');
        echo json_encode($book);
    });

    $r->addRoute('DELETE', '/books/{azonosito:\d+}', 'deleteBookHandler');

    $r->addRoute('POST', '/books', function ($query, $body, $vars) {
        // INSERT INTO books ...

        header('Content-type: application/json');
        $body['id'] = 7;
        echo json_encode($body);
    });

});

function deleteBookHandler($query, $body, $vars) {
    $id = $vars['azonosito'];
    // DELETE FROM books WHERE id = <id>

    header('Content-type: application/json');
    echo json_encode(['id' => (int)$id]);
}


// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);


$routeInfo = $dispatcher->dispatch($httpMethod, $uri);


switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        // ... 405 Method Not Allowed
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        $inputJSON = file_get_contents('php://input');
        $jsonBody = json_decode($inputJSON, true); 

        $handler($_GET, $jsonBody, $vars);
        break;
}


