<?php

/*

CRUD

Read
SELECT * FROM `products`

Read by ID
SELECT * FROM products WHERE id = 3

Create
INSERT INTO `products` (`id`, `name`, `price`, `isInStock`) VALUES (NULL, 'Példa termék 1', '2500', '1');

Update
UPDATE `products` SET `name` = 'Termék 3...', `price` = '2500', `isInStock` = '0' WHERE id = 3;

Delete
DELETE FROM `products` WHERE id = 6;
*/

$url = $_SERVER['REQUEST_URI'];
$parsedUrl = parse_url($url);

$path = $parsedUrl['path'];
$method = $_SERVER['REQUEST_METHOD'];

$connection = new mysqli('localhost', 'root', 'root', 'webshop', 3306);

if ($connection->error) {
    echo 'Szerver hiba';
    exit;
}

if ($path === '/' && $method === 'GET') {
    $result = $connection->query('SELECT * FROM products');
    $products = [];
    while ($row = $result->fetch_assoc()) {
        $row['isInStock'] = (bool) $row['isInStock'];
        array_push($products, $row);
    }

    require './views/home.phtml';
}

if ($path === '/delete-product' && $method === 'POST') {

    // SQL injection 
    // $result = $connection->query('DELETE FROM `products` WHERE id = ' . $_GET['id']);

    // prepared statement
    $statement = $connection->prepare('DELETE FROM `products` WHERE id = ?');

    $id = (int) $_GET['id'];
    $statement->bind_param('i', $id);
    $statement->execute();

    header('Location: /');
}

/*
    A listázóban alakítsd a neveket linkekké és a linkek mutassanak a
    "/termek?id=<id>" útvonalra

    Legyen egy "GET" methoddal megszólítható "/termek" útvonal, ahol a beérkező id alapján kérdezd le a db-ből
    az adott rekordot és jelenítsd meg egy template-ben (prepared statement)
*/

if ($method === 'GET' && $path === '/termek') {
    $id = (int) $_GET['id'];

    $stmt = $connection->prepare('SELECT * FROM products WHERE id = ?');
    $stmt->bind_param('i', $id);
    if (!$stmt->execute()) {
        echo 'Server error';
        exit;
    }

    $productById = $stmt->get_result()->fetch_assoc();

    if (!$productById) {
        echo 'Termék nem található';
        exit;
    }

    $productById['isInStock'] = (bool) $productById['isInStock'];

    require './views/product-single.phtml';
}

if ($method === 'POST' && $path === '/create-product') {
    $name = $_POST['name'];
    $price = (int) $_POST['price'];
    $isInStock = isset($_POST['isInStock']) ? 1 : 0;

    $stmt = $connection->prepare('INSERT 
        INTO `products` 
        (`name`, `price`, `isInStock`) 
        VALUES 
        (?, ?, ?);
    ');

    $stmt->bind_param('sii', $name, $price, $isInStock);
    $stmt->execute();
    if (!$stmt->insert_id) {
        echo 'Server error';
    }

    header('Location: /');
}

/*
    1. GET '/termék-szerkesztése?id=<id>'
        Szolgáltass egy HTML form-ot, ami fel van populálva a szerkesztett termék adataival

    2. POST 'update-product?id=<id>'
        Végezd el a termék felülírását
*/


/*
    1. Use case:
        Minden terméknek van egy termék metája is
        ProductMeta
            [
                'quantity' => 5,
                'material' => 'steel',
                'color' => 'grey'
            ]
        one-to-one relationship

    2. Use case: 
        Minden termék tartozik egy kategóriába, több termék tartozhat ugyanabba a kategóriába
        Product
            [
                'id' => 3
                'name' => 'Termék 1',
                'price' => 2000,
                'isInStock' => 0,
                'categoryId' => 2
            ]

        Category
            [
                'id' => 2
                'name' => 'Kerti szerszámok',   
                'discount' => 0.9
            ]

        one-to-many relationship

    3. Use case: 
         Minden termék tartozik egy, vagy több kategóriába

        Product
        [
            'id' => 3
            'name' => 'Termék 1',
            'price' => 2000,
            'isInStock' => 0
        ]

        Category
        [
            'id' => 2
            'name' => 'Kerti szerszámok',   
            'discount' => 0.9
        ]

        Kapcsolótábla (junction table)

        ProductCategories
        [
            'id' => 2
            'categoryId' => 2,   
            'productId' => 1
        ],
        [
            'id' => 3
            'categoryId' => 2,   
            'productId' => 3
        ]

        many-to-many relationship

*/
