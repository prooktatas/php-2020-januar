<?php

$folders = [
    [
        'id' => 1,
        'parentId' => 0,
        'value' => 'első',
    ],
    [
        'id' => 2,
        'parentId' => 1,
        'value' => 'második',
    ],
    [
        'id' => 3,
        'parentId' => 6,
        'value' => 'harmadik',
    ],
    [
        'id' => 4,
        'parentId' => 6,
        'value' => 'negyedik',
    ],
    [
        'id' => 5,
        'parentId' => 0,
        'value' => 'ötödik',
    ],
    [
        'id' => 6,
        'parentId' => 1,
        'value' => 'hatodik',
    ],
];

/*
    Node
     {
        value: string,
        children: Node[]
     }
*/

/*
1. Take the array of all elements and the id of the current parent (initially 0/nothing/null/whatever).
2. Loop through all elements.
3. If the parentId of an element matches the current parent id you got in 1.,
the element is a child of the parent. Put it in your list of current children (here: $branch).
4. Call the function recursively with the id of the element you have just identified in 3.,
i.e. find all children of that element, and add them as children element.
5. Return your list of found children.
*/

function buildTree($elements, $parentId = 0)
{
    $branch = [];
    foreach ($elements as $element) {
        if ($element['parentId'] === $parentId) {
            $children = buildTree($elements, $element['id']);
            $element['children'] = $children ? $children : null;
            $branch[] = $element;
        }
    }
    return $branch;
}

$folderTrees = parseTrees(buildTree($folders));
// $folderTrees = buildTree($folders);

function parseTrees($trees)
{
    $ret = [];
    foreach ($trees as $root) {
        $root['value'] .= ' (node érintve)';
        if ($root['children']) {
            $ret[] = parseNode($root);
        } else {
            $ret[] = $root;
        }
    }
    return $ret;
}

function parseNode($node)
{
    if (!$node['children']) {
        return $node;
    }

    $children = [];
    foreach ($node['children'] as $child) {
        $child['value'] .= ' (node érintve)';
        $children[] = parseNode($child);
    }
    $node['children'] = $children;
    return $node;
}

require 'home.phtml';
