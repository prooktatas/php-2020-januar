<?php

$method = $_SERVER['REQUEST_METHOD'];
$url = $_SERVER['REQUEST_URI'];
$path = parse_url($url)['path'];
$connection = new mysqli('localhost', 'root', 'root', 'world');

switch (true) {
    case $method === 'GET' && $path === '/':
        rootHandler($connection);
        break;
        case $method === 'GET' && $path === '/valami':
            echo 'ok';
        break;
    case  $method === 'POST' && $path === '/favourites':
        favouritesHandler();
        break;
    case $method === 'POST' && $path === '/unset-favourite':
        unsetFavoriteHandler();
        break;
    case $method === 'POST' && $path === '/upload-photo':
        photoUploadHandler($connection);
        break;
    default:
        header('Location: /');
}

function photoUploadHandler(mysqli $connection) {

    $typeMap = [
        'image/jpeg' => '.jpg',
        'image/jpg' => '.jpg',
        'image/png' => '.png',
    ];

    $extension = $typeMap[$_FILES['countryPhoto']['type']];
    if(!$extension) {
        echo 'Invalid mime type: ' . $_FILES['countryPhoto']['type'];
        exit;
    }

    $fileName = uniqid() . $extension;
    $image = file_get_contents($_FILES['countryPhoto']['tmp_name']);
    $numberOfBytesWritten = file_put_contents('./images/' . $fileName, $image);

    if(!$numberOfBytesWritten) {
        echo 'Szerver hiba';
        exit;
    }

    // Biztosítsd paraméterként a connection object-et
    // Készíts prepared statement-et
    $stmt = $connection->prepare("INSERT INTO `countryimage` (`name`, `CountryCode`) VALUES (?, ?);");

    $id = $_POST['id'];
    // Name alá kerüljön a fileName és CountryCode alá a kód
    $stmt->bind_param('ss', $fileName, $id);
    if(!$stmt->execute()) {
        echo 'Szerver hiba';
        exit;
    }

    header('Location: /');
}

function unsetFavoriteHandler() {
     // 1. Parsold ki a JSON-t, json_decode($_COOKIE['favourites'], true);

     $countryCodes = json_decode($_COOKIE['favourites'], true);

     // 2. Ha a kapott érték 1 elemű tömb, akkor
     if (count($countryCodes) === 1) {
         // állíts be sütit setcookie() function-nel 'favourites' névvel és időnek adj meg egy múltbeli
         // timestamp-et
         setcookie('favourites', '', time() - 60 * 60 * 24);
     } else {
         /*
             3. Szedd ki a tömbből a $_GET['id']-t
             4. Json encode
             5. setcookie();
         */
         $index = array_search($_GET['id'], $countryCodes);
         unset($countryCodes[$index]);

         $cookieContent = json_encode(array_values($countryCodes));
         setcookie('favourites', $cookieContent, time() + 60 * 60 * 24);

     }

     /* 6. // Redirect '/' útvonalra*/
     header('Location: /');
}

function favouritesHandler()
{
    // HA létezik $_COOKIE['favourites'] 
    if (isset($_COOKIE['favourites'])) {
        // Parsold ki a JSON tartalmat $_COOKIE['favourites']-ből json_decode($json, true)
        $favourites = json_decode($_COOKIE['favourites'], true);

        // HA már létezik $_GET['id'] a $favourites tömbben
        if (in_array($_GET['id'], $favourites)) {
            header('Location: /');
            return;
        }

        // Az így kapott adatstruktúrához push-old hozzá $_GET['id']-t
        $favourites[] = $_GET['id'];
        // Alakítsd át az adatstruktúrát JSON-ná json_encode()
        $json = json_encode($favourites);
        // Állítsd be setcookie() function-nel 'favourites' néven a JSON adatot 
        setcookie('favourites', $json, time() + 60 * 60 * 24);
        // Redirecteld '/' útvonalra
        header('Location: /');
    } else {
        // Hozz létre egy 1 elemú tömböt $_GET['id'] elemmel
        $json = json_encode([$_GET['id']]);
        // Állítsd be setcookie() function-nel 'favourites' néven a JSON adatot 
        setcookie('favourites', $json, time() + 60 * 60 * 24);
        // Redirecteld '/' útvonalra
        header('Location: /');
    }
}

function rootHandler(mysqli $connection)
{
    $favourites = [];
    // 0. HA van $_COOKIE['favourites] kulcs alatt érték, akkor
    if (isset($_COOKIE['favourites'])) {
        // 1. $_COOKIE['favourites] kulcs alatti értékét, alakítsd PHP tömbbé; $favourites = json_decode();
        $favourites = json_decode($_COOKIE['favourites']);
    }

    if ($connection->error) {
        echo 'DB error';
        exit;
    }

    $page = isset($_GET['page']) ? (int) $_GET['page'] - 1 : 0;
    $offset = $page * 10;


    // 1. Módosítsd úgy a query-t, hogy a kép neve is elérhető legyen a country adatokban 'imageName' néven
    // 2. A template-ben írd be src attribútumba az imageName alatti értéket a következő séma szerint: 
        // src="./image/<imageName>"
    $statement = $connection->prepare('SELECT 
    country.*,
    countryimage.name AS imageName
    FROM country  
    LEFT JOIN countryimage 
    ON country.Code = countryimage.CountryCode
    ORDER BY country.Code ASC
    LIMIT 10 OFFSET ?');

    $statement->bind_param('i', $offset);
    $statement->execute();

    $result = $statement->get_result();

    $countries = [];
    while ($row = $result->fetch_assoc()) {
        $result2 = $connection->query(
            'SELECT countrylanguage.Language 
                                    FROM countrylanguage 
                                    WHERE CountryCode = "' . $row['Code'] . '"'
        );

        $languages = [];
        while ($language = $result2->fetch_assoc()) {
            $languages[] = $language['Language'];
        }

        $row['languages'] = $languages;
        $countries[] = $row;
    }

    $result2 = $connection->query('SELECT COUNT(*) AS numberOfCountries FROM country');
    $numberOfCountries = (int) $result2->fetch_assoc()['numberOfCountries'];
    $numberOfPages = ceil($numberOfCountries / 10);

    require './country-list.phtml';
}
